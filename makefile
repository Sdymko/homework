all: monitore cli daemon

monitore: 
	gcc -o monitor monitor2.c dictionary.c mapwrite.c -lpcap
cli: 
	gcc -o cli_launch cli_run.c dictionary.c mapwrite.c -lpcap
daemon:
	gcc -o my_daemon daemon.c -lpcap

