#include <pcap.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <string.h>

#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>

#include "user_define.h"


#include "dictionary.h"

   FILE *fopen( const char * filename, const char * mode );

   FILE *fpt;

const char *filepath = _filepath;
const long int textsize = _textsize;
char *map;
char IP2[15]="0";
char *test_data="test";

int cfileexists(const char* filename){
    struct stat buffer;
    int exist = stat(filename,&buffer);
    if(exist == 0)
        return 1;
    else // -1
        return 0;
}

void callback(u_char *useless,const struct pcap_pkthdr* pkthdr,const u_char*
        packet)
    {
    static int count = 1;

    const struct ether_header* ethernetHeader;
    const struct ip* ipHeader;
    const struct tcphdr* tcpHeader;
    const struct udphdr* udpHeader;
    char sourceIP[INET_ADDRSTRLEN];
    char destIP[INET_ADDRSTRLEN];
    u_int sourcePort, destPort;
    u_char *data;
    int dataLength = 0;
    int counter;
    int i;

    ethernetHeader = (struct ether_header*)packet;
    if (ntohs(ethernetHeader->ether_type) == ETHERTYPE_IP) 
	{
        ipHeader = (struct ip*)(packet + sizeof(struct ether_header));
        inet_ntop(AF_INET, &(ipHeader->ip_src), sourceIP, INET_ADDRSTRLEN);
        inet_ntop(AF_INET, &(ipHeader->ip_dst), destIP, INET_ADDRSTRLEN);
	}
    printf("\nPacket number [%d], length of this packet is: %d\n", count++, pkthdr->len);
    printf("Source IP %s\n",sourceIP);
    strcpy(IP2, sourceIP);

    }





int main(int argc,char **argv)
{
    char *dev;

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* descr;
    struct bpf_program fp;        /* to hold compiled program */
    bpf_u_int32 pMask;            /* subnet mask */
    bpf_u_int32 pNet;             /* ip address*/
    pcap_if_t *alldevs, *d;
    char dev_buff[64] = {0};
    static int i =0, j=0, k=0;
    char t[1]="c";
    static int tpp;
    char buff[255];
    char buff2[255];
    char *iface_list[10][3];
char *q; 
int dev_num;
size_t sz;

    Dictionary* dic = dict_new();


    int fd = open(filepath, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
    
    if (fd == -1)
    {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }

    // Stretch the file size to the size of the (mmapped) array of char

//    size_t textsize = strlen(text) + 1; // + \0 null character
    
    if (lseek(fd, textsize, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
    }
    
    /* Something needs to be written at the end of the file to
     * have the file actually have the new size.
     * Just writing an empty string at the current file position will do.
     *
     * Note:
     *  - The current position in the file is at the end of the stretched 
     *    file due to the call to lseek().
     *  - An empty string is actually a single '\0' character, so a zero-byte
     *    will be written at the last byte of the file.
     */
    
    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
    }
    

    // Now the file is ready to be mmapped.
    map = mmap(0, textsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    char *map2 = mmap(0, textsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED)
    {
        close(fd);
        perror("Error mmapping the file");
        exit(EXIT_FAILURE);
    }
    // Un-mmaping doesn't close the file, so we still need to do that.
    
	close(fd);
	map_write (test_data,map,sizeof(test_data));

	printf ("written succes %d",2);





    // Check if sufficient arguments were supplied
/*    if(argc != 2)
    {
        printf("\nUsage: %s [protocol][number-of-packets]\n",argv[0]);
        return 0;
    }
*/
    // Prepare a list of all the devices
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }

    // Print the list to user
    // so that a choice can be
    // made
    printf("\nHere is a list of available devices on your system:\n\n");
    for(d=alldevs; d; d=d->next)
    {
	iface_list[i][0]=(char)(i+1);
	iface_list[i][1]=d->name;
	++i;        
//	printf("%d. %s", ++i, d->name);
//        if (d->description)
//            printf(" (%s)\n", d->description);
//        else
//            printf(" (Sorry, No description available for this device)\n");
    }
/*    for(k=0; k<i; k++)
	{
	printf("1: %d. %s\n", iface_list[k][0], iface_list[k][1]);
	}
*/

/*
    // Ask user to provide the interface name
    printf("\nEnter the interface name on which you want to run the packet sniffer : ");
    fgets(dev_buff, sizeof(dev_buff)-1, stdin);

    // Clear off the trailing newline that
    // fgets sets
    dev_buff[strlen(dev_buff)-1] = '\0';

    // Check if something was provided
    // by user
    if(strlen(dev_buff))
    {
        dev = dev_buff;
        printf("\n ---You opted for device [%s] to capture packets---\n\n Starting capture...",dev);
    }     

    // If something was not provided
    // return error.
    if(dev == NULL)
    {
        printf("\n[%s]\n", errbuf);
        return -1;
    }*/

    dev=argv[1];
//	printf("dev",dev);
    // fetch the network address and network mask
    pcap_lookupnet(dev, &pNet, &pMask, errbuf);

    // Now, open device for sniffing
    descr = pcap_open_live(dev, BUFSIZ, 0,-1, errbuf);
    if(descr == NULL)
    {
        printf("pcap_open_live() failed due to [%s]\n", errbuf);
        return -1;
    }
/*
    // Compile the filter expression
    if(pcap_compile(descr, &fp, argv[1], 0, pNet) == -1)
    {
        printf("\npcap_compile() failed\n");
        return -1;
    }

    // Set the filter compiled above
    if(pcap_setfilter(descr, &fp) == -1)
    {
        printf("\npcap_setfilter() failed\n");
        exit(1);
    }
*/
    // For every packet received, call the callback function
    // For now, maximum limit on number of packets is specified
    // by user.


//       printf("test");
 
    for(k=0; k<i; k++)
	{
	if (strcmp(dev, iface_list[k][1]) == 0)
	dev_num=k;
	}


sz = snprintf(NULL, 0, "%s.txt", iface_list[dev_num][1]);
q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
snprintf(q, sz+1, "%s.txt", iface_list[dev_num][1]);

j=0;

while (strcmp(t, "a"))
{
char* filename; 
filename = q;
int exist = cfileexists(filename);
 // printf("===================== t= : %s\n", t );
	
   snprintf(t, 2, "%c", map2[0]);

 //  printf("===================== t= : %s\n", t );

if(!exist)
    {
	fpt=fopen(filename, "w+");
        fprintf(fpt, "127.0.0.0 0 \n");
	fclose(fpt);
    }
else
   {
   fpt = fopen(filename, "r");
        

while (fscanf(fpt, "%s", buff)!=EOF)
{
   fscanf(fpt, "%s", buff2);
   printf("1 : %s\n", buff );

	dict_add(dic, buff,  atoi(buff2));
   }

   fclose(fpt);
}


//while (1){
   

while (j<2)
{
//    printf("\ntest2 %s\n",IP2.key2);

    pcap_loop(descr,1, callback, NULL);
    tpp = dict_get(dic, IP2);
    printf("IP : [%s] before %d packets\n", IP2, tpp);

    dict_add(dic, IP2,  (tpp+1));//(DictSearch(useless, sourceIP)+1));
    printf("IP : [%s] after %d packets\n", IP2, dict_get(dic, IP2));
    printf("IP : [%s] after2 %d packets\n", IP2, dict_get(dic, IP2));
j++;
	
}
    printf("\nDone with packet sniffing!\n");
fpt = fopen(filename, "w+");
	dict_prt(dic);
   fclose(fpt);
j=0;
}

exit(EXIT_FAILURE);
    return 0;
}
