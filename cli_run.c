/***************************************************************************//**
  @file         main.c
  @author       Stephen Brennan
  @date         Thursday,  8 January 2015
  @brief        LSH (Libstephen SHell)
*******************************************************************************/

#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "dictionary.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <string.h>
#include <pcap.h>
#include "user_define.h"

/*
  Function Declarations for builtin shell commands:
 */
int lsh_cd(char **args);
int lsh_help(char **args);
int lsh_exit(char **args);
int lsh_show(char **args);
int lsh_start(char **args);
int lsh_stop(char **args);
int lsh_stat(char **args);


/*
  List of builtin commands.
 */
struct Builtin {
  char *name;
  int (*func) (char**);
  char *comment;

};


int cfileexists(const char* filename){
    struct stat buffer;
    int exist = stat(filename,&buffer);
    if(exist == 0)
        return 1;
    else // -1
        return 0;
}

FILE *fopen( const char * filename, const char * mode );

FILE *fpt2;
FILE *fpt;

const char *filepath = _filepath;
const long int textsize = _textsize;
char *map;
    FILE *cmd;
    char result[8]="0";
    char *iface_list[10][3];
int num;

struct Builtin builtins[] = {
  { "start", &lsh_start, "Command to launch daemon in background" },
  { "stop", &lsh_stop , "Command to stop running daemon (not kill)"},
  { "show count", &lsh_show, "\"show [ip]\" count - is used to search total packets numbers received from certain ip, if no IP. if [ip] is absent command shows all measured statistics" },
  { "select iface", &lsh_cd , "\"select iface [iface]\" - used to change the sniffef interface. If [iface] is ommited you will receive list of available interfaces"},
  { "stat", &lsh_stat, "\"stat [iface]\" used to display statistik of IP values and packets. If [iface] is ommeted you will se statistics for all interfaces" },
  { "--help", &lsh_help, "" },
  { "exit", &lsh_exit, "" }

};

int lsh_num_builtins() {
  return sizeof(builtins) / sizeof(struct Builtin);
}

/*
  Builtin function implementations.
*/

/**
   @brief Bultin command: change directory.
   @param args List of args.  args[0] is "cd".  args[1] is the directory.
   @return Always returns 1, to continue executing.
 */

void get_dev_list();

void get_dev_list()
{
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* descr;
    pcap_if_t *alldevs, *d;
    int i =0;

    // Prepare a list of all the devices
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }

    // Print the list to user
    // so that a choice can be
    // made
    printf("\nHere is a list of available devices on your system:\n\n");
    for(d=alldevs; d; d=d->next)
    {
       printf("%d. %s", ++i, d->name);
        if (d->description)
            printf(" (%s)\n", d->description);
        else
            printf(" (Sorry, No description available for this device)\n");
    }
}

int lsh_cd(char **args)
{

    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* descr;
    pcap_if_t *alldevs, *d;
    int i=0, tmp=0;
char *q; 
int dev_num;
size_t sz;
    FILE *PID;

 cmd=0;
  if (args[2] == NULL) 
{
get_dev_list();

}
else
{
	  
  // Prepare a list of all the devices
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }

    // Print the list to user
    // so that a choice can be
    // made
    printf("\nHere is a list of available devices on your system:\n\n");
    for(d=alldevs; d; d=d->next)
    {
        if (strcmp(args[2],d->name)==0)
            tmp=1;
    }
	if (tmp != 0)
	{
		printf("\nYou have selected valid interface\n");

		result[8]='\0';
		//PID = popen("pgrep abcdefg -n", "r");
		PID = popen("pgrep my_daemon -n", "r");
		    while (fgets(result, sizeof(result), PID)) 
			{
		//printf("%s", result);
   			}
//        printf("%s\n", result);
         		pclose(PID);
 	  if (strcmp(result, "0") != 0) 
		{
	   	printf("daemon is already running with PID %s\n", result);

		printf("at first you need to stop daemon\n");
	/*q='\0';
	sz = snprintf(NULL, 0, "sudo kill -9 %s", result);
	q = (char *)malloc(sz + 1); 
	snprintf(q, sz+1, "sudo kill -9 %s", result);
	system(q);
 */
	return 1;
		}
    else
	{
		printf(args[2]);
	q='\0';
	sz = snprintf(NULL, 0, "./my_daemon %s & disown >/dev/null 2>&1", args[2]);
	q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
	snprintf(q, sz+1, "./my_daemon %s & disown >/dev/null 2>&1", args[2]);
	system(q);
   	printf("daemon started successfully\n");
	}


	}
	else
	  	 {
	  	 printf("\nYou have selected wrong interface\nPlease try once more\n");
		get_dev_list();
		}
	}
  return 1;
}

/**
   @brief Bultin command: start daemon for default iface wlan0.
   @return Always returns 1, to continue executing.
 */
int lsh_start(char **args)
{
FILE *cmd2;
char *q; 
size_t sz;
char error_buffer[PCAP_ERRBUF_SIZE];
char *start_iface;
    cmd2 = popen("pgrep my_daemon", "r");

    while (fgets(result, sizeof(result), cmd2)) {
//printf("%s", result);
   }
//        printf("%s\n", result);
         pclose(cmd2);
    if ( strcmp(result, "0") != 0) 
	{
   	printf("daemon is already running with PID %s\n", result);
	}
    else
	{
	start_iface=pcap_lookupdev(error_buffer);

	sz = snprintf(NULL, 0, "./my_daemon %s & disown >/dev/null 2>&1", start_iface);
	q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
	snprintf(q, sz+1, "./my_daemon %s & disown >/dev/null 2>&1", start_iface);

	//system("sudo ./monitor wlan0 & disown >/dev/null 2>&1");
// 	system("./my_daemon & disown >/dev/null 2>&1");
   	printf("daemon started successfully\n");
//printf(*pcap_lookupdev(error_buffer));
	}
	
  return 1;
}

/**
   @brief Builtin command: print help.
   @param args List of args.  Not examined.
   @return Always returns 1, to continue executing.
 */
int lsh_help(char **args)
{
  int i;
  printf("Serhii Dymko\n");
  printf("Type program names and arguments, and hit enter.\n");
  printf("The following are built in:\n");

  for (i = 0; i < lsh_num_builtins(); i++) {
    printf("  %s \t %s\n", builtins[i].name, builtins[i].comment);
  }

  printf("Use the man command for information on other programs.\n");

   return 1;
}




/**
   @brief Bultin command: stop daemon
   @return Always returns 1, to continue executing.
 */
int lsh_stop(char **args)
{
  int i;


   int fd = open(filepath, O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
    
    if (fd == -1)
    {
        perror("Error opening file for writing");
        exit(EXIT_FAILURE);
    }
   
    if (lseek(fd, textsize, SEEK_SET) == -1)
    {
        close(fd);
        perror("Error calling lseek() to 'stretch' the file");
        exit(EXIT_FAILURE);
    }
    
    /* Something needs to be written at the end of the file to
     * have the file actually have the new size.
     * Just writing an empty string at the current file position will do.
     *
     * Note:
     *  - The current position in the file is at the end of the stretched 
     *    file due to the call to lseek().
     *  - An empty string is actually a single '\0' character, so a zero-byte
     *    will be written at the last byte of the file.
     */
    
    if (write(fd, "", 1) == -1)
    {
        close(fd);
        perror("Error writing last byte of the file");
        exit(EXIT_FAILURE);
    }

    // Now the file is ready to be mmapped.
    map = mmap(0, textsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (map == MAP_FAILED)
    {
        close(fd);
        perror("Error mmapping the file");
        exit(EXIT_FAILURE);
    }
    // Un-mmaping doesn't close the file, so we still need to do that.
    
	close(fd);
	map_write ("a",map,1);
   	printf("Stop command had been sent\n");

   return 1;
}


/**
   @brief Bultin command: show [IP] count. Shows how many packets had been sent 
	if [IP] - from exect IP
	if show count - from all IP for this interface
   @return Always returns 1, to continue executing.
 */
int lsh_show(char **args)
{
  int i,k=0;
Dictionary* IP_dic = dict_new();
    char buff[255];
    char buff2[255];
    char error_buffer[PCAP_ERRBUF_SIZE];
char *q; 
int dev_num;
size_t sz;
int exist;
int count=0,tmp;
char *tp;

 /*   for(k=0; k<num; k++)
	{
	if (strcmp(pcap_lookupdev(error_buffer), iface_list[k][1]) == 0)
	dev_num=k;
	}
*/
    for(k=0; k<num; k++)
	{

	sz = snprintf(NULL, 0, "%s.txt", iface_list[k][1]);
	q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
	snprintf(q, sz+1, "%s.txt", iface_list[k][1]);


	exist = cfileexists(q);
	
	if(exist)
	    {
		fpt2 = fopen(q, "r");
		while (fscanf(fpt2, "%s", buff)!=EOF)
		      {
		      fscanf(fpt2, "%s", buff2);

		      dict_add(IP_dic, buff,  dict_get(IP_dic, buff)+atoi(buff2));
		      }

 		fclose(fpt2);
	    }
	fpt2=0;
	}
 		
	if (args[1] != NULL)
		{
 		if (dict_has(IP_dic,args[1]))
			{
			printf ("IP [%s] sended %d packets\n",args[1], dict_get(IP_dic, args[1]));
			return 1;
			}
		}


    if (IP_dic->head == NULL)
        return 0;
    while(IP_dic != NULL) {
	tp = IP_dic->head->key;
	tmp = dict_get(IP_dic, tp);
 	printf ("IP [%s] send %d packets\n",tp, tmp);       
 //       fprintf(fpt, "%s %d \n", tp, tmp);

	count = count + tmp;
        IP_dic = IP_dic->tail;
    }
    
    printf ("Totaly: %d packets\n",count);       

	  
//	dict_prt(IP_dic);
	  return 1;
}
/**
   @brief Builtin command: exit.
   @param args List of args.  Not examined.
   @return Always returns 0, to terminate execution.
 */
int lsh_exit(char **args)
{
//system("sudo ./monitor ip & disown");
  return 0;
}



//=====================================
//=======================================

int lsh_stat(char **args)
{
  int i,k=0;
Dictionary* IP_dic = dict_new();
    char buff[255];
    char buff2[255];
    char error_buffer[PCAP_ERRBUF_SIZE];
char *q; 
int dev_number;
size_t sz;
int exist;
int count=0,tmp;
char *tp;

	if (args[1] != NULL)
	{

		for(k=0; k<num; k++)
		{
			if (strcmp(args[1], iface_list[k][1]) == 0)
			dev_number=k;
		}

		sz = snprintf(NULL, 0, "%s.txt", iface_list[dev_number][1]);
		q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
		snprintf(q, sz+1, "%s.txt", iface_list[dev_number][1]);


		exist = cfileexists(q);
                printf ("===============Statistics for %s interface====================\n\n",args[1]);
 	
	if(exist)
	    	{

		fpt2 = fopen(q, "r");
		while (fscanf(fpt2, "%s", buff)!=EOF)
		      {
		      fscanf(fpt2, "%s", buff2);

		      dict_add(IP_dic, buff,  atoi(buff2));
		      }

 		fclose(fpt2);

		if (IP_dic->head == NULL)
	        return 0;
	    

		while(IP_dic != NULL) 
		    {
		    tp = IP_dic->head->key;
		    tmp = dict_get(IP_dic, tp);
	 	    printf ("IP [%s] send %d packets\n",tp, tmp);       
 //       fprintf(fpt, "%s %d \n", tp, tmp);

		    count = count + tmp;
	            IP_dic = IP_dic->tail;
		    }
    
	    printf ("Totaly: %d packets\n",count); 
	count=0;
	printf ("==========================================\n");
	printf ("==========================================\n\n");

	    }
	else
	    {
	    printf ("Statistic for this interface does not exists\n\n",count);
	printf ("==========================================\n");
	printf ("==========================================\n\n");

	    }

	}

else
{
    for(k=0; k<num; k++)
	{
	IP_dic = dict_new();
	sz = snprintf(NULL, 0, "%s.txt", iface_list[k][1]);
	q = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
	snprintf(q, sz+1, "%s.txt", iface_list[k][1]);


	exist = cfileexists(q);
        
	printf ("===============Statistics for %s interface====================\n\n",iface_list[k][1]);
	
	if(exist)
	    {
		fpt2 = fopen(q, "r");
		while (fscanf(fpt2, "%s", buff)!=EOF)
		      {
		      fscanf(fpt2, "%s", buff2);

		      dict_add(IP_dic, buff,  atoi(buff2));

		      }

 		fclose(fpt2);

		if (IP_dic->head == NULL)
	        return 0;
	    
		while(IP_dic != NULL) 
		    {
		    tp = IP_dic->head->key;
		    tmp = dict_get(IP_dic, tp);
	 	    printf ("IP [%s] send %d packets\n",tp, tmp);       

		    count = count + tmp;
	            IP_dic = IP_dic->tail;
		    }
    
	    printf ("Totaly: %d packets\n",count);
	count=0;
	printf ("==========================================\n");
	printf ("==========================================\n\n");
		//dict_free(IP_dic);
	    }
	else
	    {
	    printf ("Statistic for this interface does not exists\n\n",count);
	printf ("==========================================\n");
	printf ("==========================================\n\n");

	    }

	}	  

	  return 1;
}
}
//==================================================================
//==================================================================
/**
  @brief Launch a program and wait for it to terminate.
  @param args Null terminated list of arguments (including program).
  @return Always returns 1, to continue execution.
 */
int lsh_launch(char **args)
{
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("lsh");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    // Error forking
    perror("lsh");
  } else {
    // Parent process
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}

/**
   @brief Execute shell built-in or launch program.
   @param args Null terminated list of arguments.
   @return 1 if the shell should continue running, 0 if it should terminate
 */
int lsh_execute(char **args)
{
  int i;

char *a,*b; 
size_t sz;


  if (args[2] != NULL) {
sz = snprintf(NULL, 0, "%s %s", args[0], args[2]);
a = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
snprintf(a, sz+1, "%s %s", args[0], args[2]);
// printf("a= %s\n", a);
  }
else
	a="";

  if (args[1] != NULL) {
sz = snprintf(NULL, 0, "%s %s", args[0], args[1]);
b = (char *)malloc(sz + 1); /* make sure you check for != NULL in real code */
snprintf(b, sz+1, "%s %s", args[0], args[1]);

  }
else
	b="";

  if (args[0] == NULL) {
    // An empty command was entered.
    return 1;
  }

  for (i = 0; i < lsh_num_builtins(); i++) {
    if ((strcmp(args[0], builtins[i].name) == 0) || (strcmp(a, builtins[i].name) == 0) || (strcmp(b, builtins[i].name) == 0) ){
      return (*builtins[i].func)(args);
    }
  }

  return lsh_launch(args);
}

#define LSH_RL_BUFSIZE 1024
/**
   @brief Read a line of input from stdin.
   @return The line from stdin.
 */
char *lsh_read_line(void)
{
  int bufsize = LSH_RL_BUFSIZE;
  int position = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "lsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    // Read a character
    c = getchar();

    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    // If we have exceeded the buffer, reallocate.
    if (position >= bufsize) {
      bufsize += LSH_RL_BUFSIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}

#define LSH_TOK_BUFSIZE 64
#define LSH_TOK_DELIM " \t\r\n\a"
/**
   @brief Split a line into tokens (very naively).
   @param line The line.
   @return Null-terminated array of tokens.
 */
char **lsh_split_line(char *line)
{
  int bufsize = LSH_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char*));
  char *token, **tokens_backup;

  if (!tokens) {
    fprintf(stderr, "lsh: allocation error\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, LSH_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += LSH_TOK_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char*));
      if (!tokens) {
		free(tokens_backup);
        fprintf(stderr, "lsh: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, LSH_TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens;
}

/**
   @brief Loop getting input and executing it.
 */
void lsh_loop(void)
{
  char *line;
  char **args;
  int status;

  do {
    printf("> ");
    line = lsh_read_line();
    args = lsh_split_line(line);
    status = lsh_execute(args);

    free(line);
    free(args);
  } while (status);
}

/**
   @brief Main entry point.
   @param argc Argument count.
   @param argv Argument vector.
   @return status code
 */
int main(int argc, char **argv)
{
  // Load config files, if any.
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* descr;
    pcap_if_t *alldevs, *d;
    int i =0;

    // Prepare a list of all the devices
    if (pcap_findalldevs(&alldevs, errbuf) == -1)
    {
        fprintf(stderr,"Error in pcap_findalldevs: %s\n", errbuf);
        exit(1);
    }

    // Print the list to user
    // so that a choice can be
    // made
    for(d=alldevs; d; d=d->next)
    {
	iface_list[num][0]=(char)(num+1);
	iface_list[num][1]=d->name; 
	++num;       
    }

  // Run command loop.
  lsh_loop();

  // Perform any shutdown/cleanup.

  return EXIT_SUCCESS;
}
